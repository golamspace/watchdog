## Environment
* Ubuntu-20.04
* C++17

## Prerequisites
* g++ >= 9.4.0
* git client

## Dependency Installation
check [HOWTO.md](HOWTO.md)

## HLD
![hld](<./assets/watchdog_hld.png>)


Right now watchdog watches for configured processes status every 5s(hard coded in codebase, see: main.cpp, can be made configurable btw) and starts a process iff it finds it not be running.

### Clone Repo
Copy the project url and clone using _git_:

```shell
git clone git@gitlab.com:golamspace/watchdog.git
```

or
```shell
git clone https://gitlab.com/golamspace/watchdog.git
```

## Build Application
We are using cmake for building our application, so just  run the following commands to build the application(**assuming you have cloned the source code base in your home directory**):
```shell
cd ~/watchdog
rm -rf ./build
mkdir ./build
cd build/
cmake ..
make
```
the binary can be found in **~/watchdog/build/** directory.

## Configure application
Change the **wd_config.ini** file to add your process names
that you want to monitor and start by watchdog in the **doors** segment.
You will find some sample **PROGRAM_LIST** written there as reference.
PBI, program list has to be added in the following formation:

```shell
[doors]
PROGRAM_LIST = <absolute_path_to_program_1_executable> [<arg_1_key> <arg_1_val>], <absolute_path_to_program_2_executable> [<arg_1_key> <arg_1_val>], <absolute_path_to_program_3_executable> [<arg_1_key> <arg_1_val>]
```

sample ini configuration
```shell
[doors]
PROGRAM_LIST = /home/golam/foo -c /home/golam/foo.ini, /home/golam/bar -c /home/golam//bar.ini, /home/golam/baz -c /home/golam/baz.ini
```

## Run Application
Now that we have successfully built our **watchdog** application, we can run it using the following command:
```shell
cd ~/watchdog/build/
./watchdog -c ~/watchdog/wd_config.ini
```

## Application Log
**watchdog** is writing logs to **syslog**, so to check the logs written by **watchdog** use the following commands:
```shell
tail -f /var/log/syslog
```

same log is written in console as plaint text as well.

## Limitation
Works fine for one process monitoring. Iff multiple process is configured to monitor,
no later child process will be restarted before the first one was down and restarted by watchdog.
That is watchdog waits till the first process has any status update and restarts it, and then it restarts other.

## Convention
For coding guideline see [GUIDELINE](GUIDELINE.md).

## Contribution
To contribute see [CONTRIBUTING](CONTRIBUTING.md).

## Support
For any kind of support [Contact Me](mailto:mghhimu@gmail.com).
