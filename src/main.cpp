//
// Created by golam on 15/11/22.
//

#include <getopt.h>
#include <iostream>

#include "wd_watcher.h"
#include "wd_ini_reader.h"
#include "wd_manager.h"
#include "wd_string.h"

std::string init(int argc, char **argv) {

    std::string config_file;

    int option;
    const char *object = nullptr;

    /*
     *
     * c for configuration file path
     *
     * */
    while ((option = getopt(argc, argv, "c:")) != -1) {
        if (option == 'c') {
            if (optarg) {
                object = optarg;
            }
            config_file = object;
            std::cout << "config file: " << config_file << std::endl;
        } else {
            std::cout << "run: ./watchdog -c <your_config.ini>" << std::endl;
            abort();
        }
    }

    return config_file;
}

int main(int argc, char **argv) {
    std::string config_file = init(argc, argv);

    /**
     * initializing log manager singleton instance and logging welcome message with it
     */
    watchdog::utils::WDManager const &wd_log_manager = watchdog::utils::WDManager::get_instance();
    watchdog::utils::WDManager::dump_log("Watch Dog is watching you!");

    /**
     * load time interval to sleep after each round of process status check
     * hard coded default sleep time to 5 seconds
     */
    INIReader wd_ini_reader(config_file);
    long sleep_interval = wd_ini_reader.GetInteger("interval", "SLEEP_INTERVAL", 5);

    /**
     * pass on ini file to read program list to check in child thread to run periodically with passed static interval
     */
    std::thread worker(&watchdog::core::Watcher::watch_for_changes, watchdog::core::Watcher(config_file, sleep_interval));
    worker.join();

    return 0;
}