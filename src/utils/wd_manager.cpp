//
// Created by golam on 15/11/22.
//
#include <iostream>

#include "log/wd_syslog.h"
#include "config/wd_ini_reader.h"

#include "wd_manager.h"

namespace watchdog::utils {
    WDManager::WDManager() {
        /**
         * keeping logger name, facility, level closed for manager only
         * can be made configurable
         */
        std::clog.rdbuf(new watchdog::utils::logger::Log("watchdog", LOG_LOCAL0, LOG_INFO));
        std::cout << "Constructing watchdog log manager!" << std::endl;
    }

    WDManager::~WDManager() {
        std::cout << "Destructing watchdog log manager!" << std::endl;
    }

    void WDManager::dump_log(const std::string &log_string) {
        std::clog << watchdog::utils::logger::kLogNotice << log_string << std::endl;
        std::cout << log_string << std::endl;
    }

    WDManager &WDManager::get_instance() {
        // Since it's a static variable, if the class has already been created,
        // it won't be created again.
        // And it is thread-safe in C++11.
        static WDManager myInstance;

        // Return a reference to our instance.
        return myInstance;
    }
}