//
// Created by golam on 15/10/22.
//

#ifndef WATCHDOG_WD_STRING_H
#define WATCHDOG_WD_STRING_H

#include <string>

namespace watchdog::utils::string {

#define TRIM_LIST " \t\n\r"

    /**
     * Trim from right
     * @param source string which needs to be trimmed
     * @param t which elements to be trimmed
     * @return string
     */
    inline static std::string rtrim(const std::string &source, const std::string &t = TRIM_LIST) {
        std::string str = source;
        return str.erase(str.find_last_not_of(t) + 1);
    }

    /**
     * Trim from left
     * @param source string which needs to be trimmed
     * @param t which elements to be trimmed
     * @return string
     */
    inline static std::string ltrim(const std::string &source, const std::string &t = TRIM_LIST) {
        std::string str = source;
        return str.erase(0, source.find_first_not_of(t));
    }

    /**
     * Trim from both left and right
     * @param source string which needs to be trimmed
     * @param t which elements to be trimmed
     * @return string
     */
    inline static std::string trim(const std::string &source, const std::string &t = TRIM_LIST) {
        const std::string &str = source;
        return ltrim(rtrim(str, t), t);
    }
}

#endif //WATCHDOG_WD_STRING_H
