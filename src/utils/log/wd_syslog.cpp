//
// Created by golam on 15/11/22.
//

#include <cstring>
#include <ostream>

#include "wd_syslog.h"

namespace watchdog::utils::logger {
    Log::Log(std::string ident, int facility, int priority) {
        facility_ = facility;
        priority_ = priority;
        strncpy(ident_, ident.c_str(), sizeof(ident_));
        ident_[sizeof(ident_) - 1] = '\0';

        openlog(ident_, LOG_PID, facility_);
    }

    int Log::sync(int priority) {
        if (buffer_.length()) {
            syslog(priority_, "%s", buffer_.c_str());
            buffer_.erase();
            priority_ = priority; // default to debug for each message
        }
        return 0;
    }

    int Log::overflow(int c) {
        if (c != EOF) {
            buffer_ += static_cast<char>(c);
        } else {
            sync(priority_);
        }
        return c;
    }

    std::ostream &operator<<(std::ostream &os, const LogPriority &log_priority) {
        static_cast<Log *>(os.rdbuf())->priority_ = (int) log_priority;
        return os;
    }
}