//
// Created by golam on 15/11/22.
//

#ifndef WATCHDOG_WD_LOG_MANAGER_H_H
#define WATCHDOG_WD_LOG_MANAGER_H_H

#include <mutex>
#include <string>

namespace watchdog::utils {

    class WDManager {
    public:

        // the class is going to be neither cloneable nor movable
        WDManager(WDManager const &) = delete;             // Copy constructor
        WDManager(WDManager &&) = delete;                  // Move constructor

        // this class is not going to be assignable
        WDManager &operator=(WDManager const &) = delete;  // Copy assignment
        WDManager &operator=(WDManager &&) = delete;      // Move assignment

        /**
         * access function to create and/or get singleton instance of watchdog manager
         * @return an instance of WDManager class
         */
        static WDManager &get_instance();

        /**
         * api to dump log to syslog
         * @param log_string to dump
         */
        static void dump_log(const std::string &log_string);

    private:
        WDManager();

        ~WDManager();
    };
}

#endif //WATCHDOG_WD_LOG_MANAGER_H_H
