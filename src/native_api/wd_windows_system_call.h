//
// Created by golam on 15/10/22.
//

#ifndef WATCHDOG_WD_WINDOWS_SYSTEM_CALL_H
#define WATCHDOG_WD_WINDOWS_SYSTEM_CALL_H

#include "wd_system_call.h"

namespace watchdog::native_api {
    class WindowsSystemCall : public SystemCall {
    public:
        WindowsSystemCall() = default;

        ~WindowsSystemCall() override = default;

        void get_process_status(std::string process_name, std::string process_start_command) override;
    };
}
#endif //WATCHDOG_WD_WINDOWS_SYSTEM_CALL_H
