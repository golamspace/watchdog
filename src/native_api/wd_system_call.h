//
// Created by golam on 15/10/22.
//

#ifndef WATCHDOG_SYSTEM_CALL_H
#define WATCHDOG_SYSTEM_CALL_H

#include "string"

namespace watchdog::native_api {
    class SystemCall {
    public:
        SystemCall() = default;

        virtual ~SystemCall() = default;

        virtual void get_process_status(std::string process_name, std::string process_start_command) = 0;
    };
}
#endif //WATCHDOG_SYSTEM_CALL_H
