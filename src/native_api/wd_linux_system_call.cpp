//
// Created by golam on 15/10/22.
//

#include <stdlib.h>
#include <unistd.h>
#include <iostream>
#include <csignal>

#include "wd_linux_system_call.h"
#include "wd_manager.h"

#define PIDOF_COMMAND "pidof -s "
#define BUFFER_LENGTH 512

namespace watchdog::native_api {

    void run_new_process(std::string process_name, std::string process_start_command) {
        /**
         * ignore signals raised to child process so that they don't wait for parent process and remain zombie
         * business logic wise signal handler can be added
         */
        signal(SIGCHLD, SIG_IGN);

        pid_t pid;
        pid = fork();

        if (pid == -1) {
            /**
             * pid == -1 means error occurred
             */
            watchdog::utils::WDManager::dump_log("Fork failed, error occurred!");
            exit(EXIT_FAILURE);
        } else if (pid == 0) {
            /**
             * pid == 0 means child process created
             * getpid() returns process id of calling process
             * getppid() returns process id of calling process
             */
            std::cout << "child process, PID: " << getpid() << ", parent process, PPID: " << getppid() << std::endl;

            /**
             * execl() replaces the current process image with a new process image
             */
            execl(process_name.c_str(), process_start_command.c_str(), NULL);

            /**
             * iff we are here, some error has occurred in the child process, as execl will only return iff any error occurs
             */
            exit(0);
        } else {
            watchdog::utils::WDManager::dump_log("Child process started!");
        }
    }

    void LinuxSystemCall::get_process_status(std::string process_name, std::string process_start_command) {
        /**
         * getting logger object
         */
        watchdog::utils::WDManager const &wd_log_manager = watchdog::utils::WDManager::get_instance();

        /**
         * getting process status by running linux command and piping to buffer
         */
        char buf[BUFFER_LENGTH];
        std::string process_check_command = PIDOF_COMMAND + process_name;
        FILE *cmd_pipe = popen(process_check_command.c_str(), "r");
        fgets(buf, BUFFER_LENGTH, cmd_pipe);

        /**
         * converting buffer string to pid type
         */
        pid_t pid = strtoul(buf, NULL, 10);

        /**
         * process id of init program is 1
         * and so any other process would have pid greater than 1 iff it's running
         */
        if (pid > 1) {
            watchdog::utils::WDManager::dump_log("A process is already running with name: " + process_name + "!");
        } else {
            watchdog::utils::WDManager::dump_log("No process is running with process name: " + process_name + "!");
            watchdog::utils::WDManager::dump_log(
                    "Starting " + process_name + " using command: " + process_start_command);

            /**
             * current process is replaced by new process image, that was our flaw
             * because we don't want to replace the program,
             * rather we want to run a new process with new program
             * so now we are forking a brand new process and then replacing it's image
             */
//            execl(process_name.c_str(), process_start_command.c_str(), NULL);

            run_new_process(process_name, process_start_command);
        }

        pclose(cmd_pipe);
    }
}