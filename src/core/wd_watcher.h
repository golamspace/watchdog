//
// Created by golam on 15/10/22.
//

#ifndef WATCHDOG_WD_WATCHER_H
#define WATCHDOG_WD_WATCHER_H

#include <thread>

#include "wd_linux_system_call.h"

#include "wd_ini_reader.h"

namespace watchdog::core {
    class Watcher {
    public:
        Watcher(std::string config_file, int sleep_interval);

        void watch_for_changes();

    private:
        std::string config_file_;
        int sleep_interval_;
        std::shared_ptr<watchdog::native_api::SystemCall> system_call_{nullptr};
    };
}

#endif //WATCHDOG_WD_WATCHER_H
