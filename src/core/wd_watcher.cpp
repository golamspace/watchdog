//
// Created by golam on 15/10/22.
//

#include <iostream>
#include <sstream>
#include <string>
#include <unistd.h>

#include "wd_string.h"

#include "wd_watcher.h"
#include "wd_manager.h"

namespace watchdog::core {

    Watcher::Watcher(const std::string config_file, int sleep_interval) {
        config_file_ = config_file;
        sleep_interval_ = sleep_interval;
        system_call_ = std::make_shared<watchdog::native_api::LinuxSystemCall>();
    }

    void Watcher::watch_for_changes() {
        while (true) {
            /**
             * config file is reloaded and parsed to get new program list
             */
            INIReader wd_ini_reader(config_file_);
            std::string program_list = wd_ini_reader.Get("doors", "PROGRAM_LIST", "");
            std::vector<std::string> program_list_to_watch;
            std::stringstream program_list_string_stream(program_list);
            std::string token;

            while (std::getline(program_list_string_stream, token, ',')) {
                program_list_to_watch.push_back(watchdog::utils::string::trim(token.substr(0, token.size())));
            }

            for (auto app_entry: program_list_to_watch) {
                watchdog::utils::WDManager const &wd_log_manager = watchdog::utils::WDManager::get_instance();
                watchdog::utils::WDManager::dump_log("App Entry: " + app_entry);

                std::vector<std::string> program_artifacts;
                std::stringstream program_artifacts_string_stream(app_entry);
                std::string artifact_token;

                while (std::getline(program_artifacts_string_stream, artifact_token, ' ')) {
                    program_artifacts.push_back(
                            watchdog::utils::string::trim(artifact_token.substr(0, artifact_token.size())));
                }

                if (program_artifacts.empty()) {
                    watchdog::utils::WDManager::dump_log("No app name found!");
                } else {
                    std::string app_name = program_artifacts.front();
                    watchdog::utils::WDManager::dump_log("App to watch[" + app_name + "]");
                    system_call_->get_process_status(app_name, app_entry);
                }
            }

            sleep(sleep_interval_);
        }
    }
}
