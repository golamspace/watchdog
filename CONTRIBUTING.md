## Introduction

>Thank you for your consideration to contribute in watchdog.
> It's people like you that make things better.

## Reporting a bug or a feature
Please, use for any kind of issue reporting and don't forget to specify the tracker, ie: bug/feature.
> Please be specific. Use _**[[watchdog]][[tracker]]**_ as prefix of your issue title.
> See [this_bug_report_issue](https://gitlab.com/golamspace/watchdog/-/issues/1)
> 
> or
> 
> [this_feature_request_issue](https://gitlab.com/golamspace/watchdog/-/issues/2) as sample.

## Contributing to source
Create new branch from **_develop_** and name it _**feature/issue_number**_ or _**bug/issue_number**_, eg: **_bug/1_**.
> Read and follow [Coding Guideline](GUIDELINE.md) properly before starting coding

## Commit Message Convention
> {tracker}[watchdog]: {commit headline}
> 
> - {commit description body 1}
> - {commit description body 2}
> 
> resolves: #{issue_id}
> 
> ref: #{issue_id}

sample commit message:
> feature[watchdog]: added native os api interface
> 
> - added implementation for linus system
> 
> resolves: #100