### Dependencies  
*ubuntu 20.04 LTS*  
1. **cmake** installation:  

   Installation from ubuntu repo:  
   
   ```sh  
   sudo apt install cmake 
   sudo apt remove cmake
   ```
    or installation from the source:  
    
   ```sh  
   sudo apt purge cmake
   wget https://cmake.org/files/v3.13/cmake-3.13.3-Linux-x86_64.tar.gz` (https://cmake.org/download/)  
   tar -xzf cmake-3.13.3-Linux-x86_64.tar.gz
   cd cmake-3.13.3-Linux-x86_64/
   sudo cp -r bin /usr/
   sudo cp -r share /usr/
   sudo cp -r doc /usr/share/
   sudo cp -r man /usr/share/
   ```
 
2. **build-essential** installation:  
   
   ```sh  
   sudo apt update  
   sudo apt install build-essential
   ```