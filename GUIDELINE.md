**Coding style guideline**

- Code should be compiled with all compiler flags on and each code must compile without any warning.
- CamelCase for class and structure names.
- snake_case for variable, method and file names.
- `apple` should be named `apple`, not `fruit`. eg: if there are several type of sessions,
  like even if `call_session`, `connection_session`, `user_session` are local to separate functions,
  don't just name them `session`
- we all know enum values start with `0` if not specified,
  yet assign values explicitly to each enum entries even if your enum value starts with `0` to increase readability
  eg, use this:
  ```text
          enum CallState {
              INVALID = 0,
              ACCEPTED = 1,
              RINGING = 2,
              PROGRESSING = 3,
              ESTABLISHING = 4,
              ESTABLISHED = 5,
              CLOSING = 6,
              TERMINATED = 7,
              CANCELLING = 8,
              FAILED = 9
          };
  ```
  instead of:
    ```text
            enum CallState {
                INVALID,
                ACCEPTED,
                RINGING,
                PROGRESSING,
                ESTABLISHING,
                ESTABLISHED,
                CLOSING,
                TERMINATED,
                CANCELLING,
                FAILED
            };
    ```     
    trust me, this will save your time at debugging if you are cross-matching logs with entries from codebase.
- Use 4 spaces instead of tab.
- Never use leading underscore in any kind of identifier in any scope.
- `using namespace` is not allowed in global scope. The only place it can be used is inside a function scope. Always use full namespace scope resolution even if it is within the same namespace, eg: even if you use `foo::bar::func()` somewhere in `foo::baz`, don't write `bar::func()`, write `foo:bar::func()` instead.
- `#include` header ordering: standard C headers first, then standard C++ headers, then any 3rd party headers and finally own headers. Each section should be sorted alphabetically. No matter what `example.h` or `example.hpp` should be the last entry in `example.c` or `example.cpp` respectively.
- Never include headers in your header files(.h or .hpp) if you need them only on your implementation files(.c or .cpp), include them in implementation files(.c or .cpp) instead.
- Never include header in your parent class or interface class if you need them only on a specific child class or implementation class, use them to that specific class files.
- Opening curly brace `{` is on the same line of loop, method or condition definition.
- Pointer symbol is with identifier name, not with type name, i.e. `int *a`, not `int* a`.
- try to catch every exception thrown by any api you are calling
- try to handle all error codes returned from the apis you are calling
- never forget to update [CHANGELOG](CHANGELOG)